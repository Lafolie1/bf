package main

import "core:fmt"
import "core:os"
import "core:math"
import "core:unicode/utf8"

main :: proc()
{
	buf: [0xffff]byte

	count, err := os.read(os.stdin, buf[:])
	assert(err == os.ERROR_NONE, "Invalid input")

	code := string(buf[:count])
	dat := new([30000]byte)
	pc := 0
	dc := 0

	for pc < count
	{
		switch utf8.rune_at_pos(code, pc)
		{
		case '>':
			dc = math.min(30000, dc + 1)
		
		case '<':
			dc = math.max(0, dc - 1)

		case '+':
			dat[dc] += 1

		case '-':
			dat[dc] -= 1

		case '.':
			fmt.printf("%c", dat[dc])

		case ',':
			b: [1]byte
			_, err := os.read(os.stdin, b[:])
			assert(err == os.ERROR_NONE, "do'h")
			dat[dc] = b[0]

		case '[':
			if dat[dc] != 0
			{
				break
			}

			depth := 1
			for depth > 0
			{
				pc += 1

				assert(pc < count, "Syntax error: missing ]")
				switch utf8.rune_at_pos(code, pc)
				{
				case '[':
					depth += 1
				case ']':
					depth -= 1
				}
			}

		case ']':
			if dat[dc] == 0
			{
				break
			}
			
			depth := 1
			for depth > 0
			{
				pc -= 1
				assert(pc >= 0, "Syntax error: missing [")
				switch utf8.rune_at_pos(code, pc)
				{
					case '[':
						depth -= 1
					case ']':
						depth += 1
				}
			}
		}

		pc += 1
	}

	fmt.println("\ndone")
}
